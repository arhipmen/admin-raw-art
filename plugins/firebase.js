import * as firebase from 'firebase';

// Your web app's Firebase configuration
let firebaseConfig = {
  apiKey: "AIzaSyAXi4F5pepiqUUZJu01nj4WvQ4zSvC19HM",
  authDomain: "raw-art-39aa1.firebaseapp.com",
  databaseURL: "https://raw-art-39aa1.firebaseio.com",
  projectId: "raw-art-39aa1",
  storageBucket: "raw-art-39aa1.appspot.com",
  messagingSenderId: "129090401922",
  appId: "1:129090401922:web:f535857b7557a75d60e69b"
}
// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig)
}

export default firebase
